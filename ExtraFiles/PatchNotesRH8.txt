Ravenhearst 8

v8.0.0.1

-Added Redbeards Active Ingredients
-Updated Spherecore
-Updated Take and Replace
-Added Riles UI
-Added Old Block Upgrades Modlet



v8.0.0.3

-Added Biome Specific Ores
-Updated Bdubya Vehicles
-Merged Worm Items
-Added Yakovs Quality Code
-Added Yakovs Menu Music Code
-Stations Now Require Fuel
-Merged Food and Food Expanded Folders into One Folder Called ExpandedFood
-Fixed Incorrect Basic Quest Numbering
-Fixed Tool Slots in Forge and BS Forge
-Items should properly highlight if you have the materials to craft them
-Unlocks Should No Longer Null When Clicking Item Info
-ReAdded Action Skill System
-ReAdded Blunderbuss and Ammo
-Added Active Ingredients by Yakov
-Removed References to Plasma and Junk Batons
-Fixed Title and Description on Water Unit Quest
-Added Loading Screens
-Added Journal Tips and Localization
-Added Harvestable Lanterns and Flashlights
-Fixed Incorrect Repair Description on Compound Crossbow
-Fixed improper wording in High End Metal description
-Fixed Backpack and Luggage Models Floating When Placed
-Added Missing Weapons to Repair with Advanced Repair Kit (Bat, Kukri)
-Added Air Drop Zombies
-Added Removed Challenges
-Increased Zombie Spawning for Treasures
-Removed Unneeded Categories from Decor Table and Electric Table
-Added Decor Table


v8.0.0.4

-Added Vaccination Clinic by MrJoseCuervo
-Added Meth Lab by MrJoseCuervo
-Added Apartments by MAlice
-Added New Calibers 5.56 and 45
-ReAdded Irrigation System Code by Yakov
-Updated RH Core
-Updated Sphere Core
-ReAdded Farm Plot Light Requirements
-Updated Farming Modlet to reflect irrigation and light changes
-Fixed Worm and Grub Harvest Insanity
-Added Dirty Bandages
-ReAdded Dinosaur Fossil
-Readded Carpets
-Adjusted Buffs on Broken and Sprained Arms and Concussions

v8.0.0.5

-Added Yakovs Performance mod
-Added Blood Moon Lasts Until Dawn Modlet
-Added Ravenhearst Buffs
-Ember Piles can now set you on fire if stepped on
-Added Lore Notes
-Fixed Large Signed Storage and Secure Storage Sizes
-Fixed incorrect starting class quality numbers from 3 to 300
-Added Icons for All Class Books
-Added Spheres fixes for Linux. Damn you linux players!
-Added Video Options for RH by Yakov
-Removed set of our Stash Backpack buttons until they work. Vanilla only for now
-Updated Trader Tip
-Updated Ammo Press Tip
-Added New Calibers to Ammo Press
-Removed Unused Explosives Class References
-Added Telrics New Fishing


v8.0.0.6

-Fixed Forge Tool Slots Not Appearing
-Aligned Crosshair Menu
-Added Bent Nails to More Wood Sources
-Lowered probabilities on grown RH crops in plots
-Added Better Fish Dishes to Master Chef Perks
-Added proper unlock info to White and Red Meats
-Removed Extra set of Stash Buttons
-Added Missing Icons for 45 Ammo
-Lowered Biscuit by 2 Food
-Removed Extra Mold


v8.0.0.7

-Fixed Ammo Press Tools Not Showing
-Fixed Forge Tool Slots Not Showing
-Updated Active Ingredients by Yakov
-WFU has the correct recipe now
-Fixed Missing Icons for Scrap Axe andPickaxe
-ReAdded Hazmat Suits
-ReAdded Oil Refinery
-ReAdded Phone Quests
-ReAdded Armor Bench
-Added Sprains Modlet by 13erserk
-Updated Spherecore
-Removed PK/ZK Hud
-Added Trader Traps by evilraccoon
-Added Campsite Cave POI by evilraccoon
-Added Burnt Shell POI by evilraccoon


v8.0.0.8

-ReEnabled XP Notification So It Works Properly With Settings
-Added Empty Pallet and Pallet Stacks
-Added Bowl to Ramen Recipe
-Air Drop Loot Lists Properly Named This fix will not work on existing air drops
-Wording Corrected on Living Off the Land Perk
-Capitalized L in Localization in Wellness Modlet Causing Linux Issues
-Fixed Sandwich Localization
-Fixed Incorrect number of meat in grilled pork and chicken recipes
-Dough Now Yields 3 Per Craft

-Removed The Following Foods
- Baguette
- Extra Steak Sandwich
- Cream Cheese
- Salmon Cream Cheese Sandwich
- Pork Cream Cheese Sandwich
- Pats Steak
- Ham and Cheese Sandwich
- Bagels
- Cream

-Increased Stats on All Smoker Foods
-Fixed Quality Numbers on Fishing Items
-Raised Stats on Tuna BLT
-Reduced Stats on Fish Tacos
-Massive Fixes and Changes to Fishing Table Areas
-Removed Buff From Milk
-Removed Milk and Butter from Trash


v8.0.0.9

-Added Yakovs Fixes for Settings Not Being Saved Between restarts
-Added RH Lock Slots and Backpack Buttons Fixed and Saving Between Restarts by Yakov
-Fixed Knife Tool Not Showing in Food Prep Table
-Fixed Wood Master Not Giving Bent Nails
-Fixed Quest Item Warning Localization
-Lowered Probabilities on Male and Female animal Harvests
-Rabbits Now Give Red Meat
-Slightly Increased Bone Fragment Counts on Animals
-Removed Fuel Values From Our Weapons to Match Vanilla
-Fixed Nulling Windows for Coffeemaker, Microwave, Toaster, Stoves

-Complete redesign of farming with localization

	No LOTL Perks

	Unfertilized
	Normal Grow Time, 1 Seed, 1 Crop

	Fertilized
	Quicker Grow Time, 1 Seed, 2 Crops
	
	LOTL Perks Descriptions should Now Be Correct

-Added Ravenhearst Sounds For Future HookUp
-Fixed Redundant Mixed Veggies Recipes


v8.0.0.10

-Added New POI War Memorial by evilraccoon
-Added Schematics That Were Missing From Journal Quest Completed
-Fixed Missing Trader Loading Tip
-Added 0 Nail Harvest to Zombie Barricade
-Added Blue Rad Smoke to T5 Vanilla Pois. Some May Be Missed
-Added Yakovs Fixed Up and Updated Wellness Modlet
-Updated Spherecore
-Added BepinEX Files
-Removed Unneeded Menu Music Folder
-Updated Hazmat Suits to Work with Biome Radiation
-Updated Radiation to work in Wasteland
-Added Sinders Fixes for Vanilla POIs
-Fixed Localization on Snowballs
-Fixed Backpack Size Overlapping Forges
-Fixed Cutting Fish Requiring Food Prep Table
-Fixed Localization On Cobblestone Shape
-Adjusted Durability on Primitive Tools
-Removed Archery Mod
-Added Ammo Disassembly to the Ammunition Press
-Added Unlock Option to Gothic Statue Block
-Removed Building Tab from BS Forge
-Fixed Industrial Nailgun Using Bricks
-Fixed Lathe Recipes Not Showing
-Mining Machine Has Been Completely Overhauled
-Added Lockpick Model
-Added Combine Window
-Added Various XML to Ravenhearst_DLLXMLS
-Added OCB Map Waypoint Modlet. Adds More Icons
-Changed Recipe for Bacon and Eggs
-Removed Corn Grits
-Removed Iced Coffee
-Added Splash Screen Enhancement by Redbeard
-Added Optimized Mining Machine and Smitty Doors Assets by Yakov and Redbeard
-Added More Options for Zombie Max
-Added 20 Hr Option for Daylight


v8.0.0.11


-Fixed Unlocks For Tungsten and Chrome Arrows
-Rewrote Classes For Pipe Weapons
-5 Points Given at Start of Class
-10 Points Given Upon Completion pf Class
-Increased XP Given at End of Class
-Increased Coins Given at End of Class
-Arrow Shafts and Heads Reinstated
-Backpack Perks Now Located Under Survival Action Skill
-Fixed Localization on Willy Jeep Schematic
-Added Nails to New Building Upgrades
-Added Screws to Metal Building Shapes and Frames
-Scrap Tools Now Given to Building Classes
-Radiation Tooltips Updated
-BepinEx Folders Reorganized


v8.1.0.1

-Updated to Exp 20.1b5
-Updated RH Core
-Updated Spherecore
-Balance of KP and Ink in desks/filing cabinets
-Gun Part Crafting Added to Lathe
-Removed Empty Crafting Groups from Tanning Rack, Food Prep Table, WFU and Farm Table
-Added more Food Amount to Grilled Potato and Mashed Potato
-Added Redbeards Last Words Modlet
-Added New Menu Courtesy Redbeard and Yakov
-Added Yakovs 4x4 Irrigation Pipe
-Updated Loot and Recipes to reflect 20.1 changes
-Added Yakov Fixes for Wellness
-Added RH Credits Screen to Menu by Redbeard and Yakov
-Acid and Seed Packs Increased
-Fixed Localization on Automatics Quest
-Changed Description on Art of Mining Pallets
-Lowered Prob on Ink and KP
-Localization Fix for Radiation Buff
-Broken Lanterns No Longer Drop Working Ones
-Added Mean Clouds Cannabis System
-Updated Bad Medicine Modlet
-Added NPC raiders, Soldiers and Spiders. Going to need MASSIVE feedback on these


v8.1.0.2

-Updated to Stable 20.1b6
-Added Trader Rekt POI by evilracc0on
-Added zt Functional Elevator
-Added Trader Joel POI by evilracc0on
-Butter Craft Now Yields 2
-Fried Egg Craft Now Yields 2
-Fox Meat Harvests Fixed
-Spider Harvests Fixed
-Bong Now Crafted in Forge Properly
-ReAdded Our Old Trader Balances Modlet for item removals, reset intervals etc
-Removed Bundles
-Reworked Perk Books
-All Containers Now Have a Chance to be Empty
-Updated Wellness
-Updated Perma Death
-Updated Sphere Core
-Added Yakovs Survival Journal End Quest Line
-Cheese now Yields 3
-Meatball Sub now subtracts 18 Water, 12 Stam and adds 65 Health
-Added Vinegar to Loot and missing recipe
-Updated Compost Quest to Proper Amounts and recipes
-Integrated BepinEx Mods Into Mods Folder
-Male Rabbits No Longer Drop Both Male and Female Babies
-Removed Writable Storage from PWB
-Lever Rifle Ammo Type Fixed
-Mixed Veggies Now Made in Food Prep Table
-Grilled Mushroom and Corn now Give 10 Food
-Added Yakovs Balances for Bandit and Zombie Spawning


v8.2.0.1

-Updated to a20.2b2 Stable
-Corn Chips Now Gives 7 Food
-Updated RH Core
-Updated Xyth NPC
-Updated Raiders and Bandits
-Fixed Nulling Air Drops Due to Cop Model
-Fixed Zombie Hands
-Redid Bandit Spawning
-Removed Nuke Bandit
-Hazmat Suit Now an Armor Slot
-Blood Moons Should Now Last All Night
-Loot Templates Rebalanced for Gun Parts
-Added Fixed Trader Rekt POI by evilraco0n
-Added ZZTongs MANY POI Modlet
-Added All Rh POIs
-Added Night Terrors fixed by Yakov
-Fixed Nulling Tiered zombie Spawns
-ReAdded Writeable Storage
-Fixed Bundle Quality
-Fixed LOTL Localization Unlock For Salad
-Buckets Now Need Filtrated
-Increased Price of Hazmat Suits


v8.2.0.2

-Fixed Trader Tip
-Added Fixed Trader Pois
-Added Yakovs Fixes for Trader Buried Supplies
-Added Redbeards Adjustments for writeTo
-Added Yakovs Zombie Spawning Balances
-Added Yakovs Fixes for Core and Biome Scaling
-Added Wellness Tweaks


v8.2.0.3

-Removed Door Frame Block When Picking Up Barricades
-Reduced Zombie Spawns
-Gently Placed Trader Rekt to the side
-Nerd Poling Fixed
-Quest Craft Warning Sound Now Plays


v8.3.0.1

-Updated to 20.3b3
-Removed Throw from Spears. Added Power Attack
-Added Destroy AS XP to Mining and Construction
-Removed Scrap Knife from Blade Quest
-Fried Eggs Now Made in Survival Campfire
-Added Javelin for Throwing
-Removed Craft Timer Bonus From Brick Mold
-Bacon and Eggs Now Gives 1 Wellness
-Vitamin Duration Extended
-Arrow and Bolt Ammo Reverted Back to Original
-Archery Crafting Progression Fixed to Reflect Removal of Archery Modlet
-Fixed Name of Telescopic Baton
-Increased Gun Parts in Loot
-Guns can no longer be looted over 100 Q. They must be crafted
-Complete loot quality overhaul. Should find much less higher tiers
-Added Medical Items to Vending
-Scrap Tools Can Now Be Found In Loot Iron and Steel Tools Moved Up a Tier in Loot
-Adjusted Spawns for End Tier Quest
-Removed Fishing
-Extended Descriptions for Fortitude and Agility
-Dysentery Now Triggers Instantly
-Updated NPC Core
-Removed Spear Books
-Rebalanced Ink in Loot
-Fixed Descriptions on Agility and Fortitude
-Hobo Stew and Sham Chowder Now Made in Stoves
-Vinegar Schematic Name Fixed
-Death XP Penalty Can Not Exceed 75 Percent (3 Deaths)
-First Aid Storage Size Reduced
-Backpack Storage Size Increased
-Added Dunkin Donuts By RiahRex
-Added Localization For NPC Buttons
-Adjusted Costs to Hire NPCs
-Fix Textures for Mining Machine
-Increased Timers on Mining Machine
-2 Turrets Can Now Be Placed with Magazine Unlocks
-PWB Now Takes 5 Seconds to Pick Up
-Lowered Surface Ores
-Complete Rebalance of Loot Quality
-Fixed Trader Quality Issue
-Added Heatmap Values to RH Stations
-Added Bent Nails to the Following
	Picnic Table
	Nightstands
	Pool Tables
	End Tables
	
-Added Smoke and Crates to New T5's
-Added Museum Artifacts Found in T5 Crates
-Added Multiple Firearms Loot Only to T5 Crates
-Added Multiple Melee Weapons Found in T5 Crates Only
-Added Boat
-Added Hang Glider
-Added Helicopter Found in T5 Crates Only
-Added Armored SUV Found in T5 Crates Only
-Added Coal to Snow Biome
-Moved Aluminum to Wasteland
-Fixed Radiation and Hazmat Issues For Damage
-ReAdded Irrigation Pipe Recipes
-Final Backpack Recipe Should Now Unlock All Slots
-Removed Mod Slots from Hazmat


v8.3.0.2

-Updated Version Number
-Storage Now Works in Drone
-Guns Can Now be Crafted to 1000


v8.3.0.3

-Lowered XP Needed to Level Most Action Skills. Levelling Them should be slightly Quicker.
-Removed NPC Zombie
-Lowered Spawn Probabilities on All NPCS
-Removed Beaker From Rain Catcher Recipe
-Added Multiple POI Vanilla Edits For Billboards by Evilracc0on
-Fixed All Weapon and Tool Perk 4 and 5 Crafting Unlocks
-Increased Health on Night Terrors
-Moved Night Terrors in Desert/Snow/Forest to Post GS 100
-Moved Night Terrors in Wasteland to GS 25
-Increased Acid in Loot
-Added Pallets for Feathers, Hides, Sticks, Twine
-Increased Price on Old Stoves
-Jerky Quest Now Gather
-Increased Amount of Tarps in Loot
-Fixed Null on Javelin
-Removed Hunter Trap 1 Causing Nulls
-Added Javelin and Pitchforks to Javelin Quest
-Fixed Improper Meat To Hire Wolf Loot Quality Fixes Again
-Increased Health On White Picket Fences


v8.3.0.4

-Removed Mentions of Fishing
-Wellness Stat Loss Now a 1 from injury
-Stamina Regen bottoms out at 1 per regen per sec at 0 Water instead of No Regen
-Adjusted Stats on All Scrap Tools
-Scrap Knife Now Unlocked via Huntsman Perks Level 1
-Adjusted Recipe for Pitchforks
-Slightly Increased Animal Fat
-Fixed Rain Collector Quest
-Added Tarp to Rain Collector Craft
-Impact Driver Can Now Repair and Upgrade Blocks
-Crowbar and Claw Hammer Will Now Reduce Take and Replace Timer


v8.3.0.5

-Grumsticks Now Scrap to Plastic
-Stoves and Ovens Now Craftable Broken
-Named the Stove and Wood Burning Stove Kits Properly
-All Stoves Should Be Upgradeable


v8.3.0.6

-Lowered Prob for Tiered Zombies
-Added Delay Between Enemy Animal Spawns
-Fixed Javelin Unlocks
-Added Animal Fat To Human Corpses
-Iced Tea Increased to 50 Water
-Iced Tea Now Uses Dried Leaves
-Iced Tea Now Uses Boiled Water
-Animal Fat Added To Rabbits Chickens and Zombie Bears
-Increased XP Gain and All Bears
-Moved Frame Shapes to Benches
-Increased Cost of Reinforced Scrap Frame
-Lowered Bone Fragments in Animals
-Lowered Bone Fragments on Gore
-Tree Sap Can Be Used to Make Glue in Chem Station


v8.3.0.7

-Javelin Attack Added
-Removed Unused Bow From Quest Line
-Added Redbeards Loading Screen Tweaks
-Removed Butcher
-Fixed Hazmat Pants Switch Out Issue
-Fixed Hazmat Gear Quality Number Issue
-Upped Probability for Novels
-Lowered Chance for Perk Books


v8.3.0.8

-Added Redbeards No Delay on Inventory Open
-Fixed Repair Kit Requirements on New Weapons
-Increased Durability on Javelin


v8.3.0.9

-Oil Refinery Timer Fix
-Fixed T5 Loot
-Increased Chance of Research Desk Schematic in T5 Loot
-Armor and Hazmat Fixes


v8.3.0.10

-POI Tag and Size Fixes 1


v8.3.0.11

-Added Several More Pois
-Fixed Fertilized Seeds. Should Now Be Much Faster
-Fixed Wrong Irrigation Pipe Material
-Added Irrigation Pipe Unlocks to LOTL5
-Vinegar can now be Unlocked
-Linux Files Added
-Added Rembomys Medical Kit


v8.3.0.12

-Added Rembomy linux bepinex fix
-Legendary guns can now craft to 100
-Rocket launcher now will level when you buy the demo perk
-Restored missing vehicle parts to recipes
-Added crusts to meat and beer and mushroom pies
-Wood burn stove now acts like a survival campfire
-Renamed purchasable and craftable stoves
-Renamed modern stove repair kit
-Removed bow parts from loot
-Removed machete parts from loot
-Fixed negative stamina values on BLT
-Crossbow repeater now unlocks on ranger book completion
-Fixed steel and iron knuckles unlocks and naming
-Removed alternate microwave model forcing quest to not complete
-Added more description to the class completion text
-Increased XP on Terrors
-Increased Loot Prob on Terrors
-Added Mortar to Building Repairs
-Fixed Screws Icon
-Added RH Pois Thanks to Constip Fox and Romper

====================================================Version 8.3.1 Release=========================================================


v8.3.1.1

-Added New AGF/Riles Hud with Enhancements by Dracconis69
-Added Tags to Armor for Bench Unlocking Thanks to Dracconis69
-T5 POIs will now be Radiated
-Mechanic Bench Now Shows Tool Slot
-Ministers Tree House Quest Fix 
-Reduced Texture Sizes on Radios, Ores, Police Items
-Added Wellness Journal Tip
-Increased Cost of Hiring NPCs
-Fix Quest Marker on SlavCo
-Fixed Improper XP Rewards for Tiered Zombies
-Fixed Improper Rangers Guide Unlocks
-Trader no longer sells guns over 100
-Added Fiber Cordage to Workbench and Table Saw Recipes to Match Quest
-Updated Beer Count for Insect Trap Quest
-Removed Unused Football Groups Causing Issues in Quests in POIs
-Removed Armored SUV from Loot
-Added Code to re-enable "gfx st budget 0" in the console thanks to Yakov and Redbeard to clear blurry textures
-Removed concrete accelerator
-Steel Polish Will Now Unlock
-Fixed incorrect spelling on Utility Zombie Groups in POI
-Removed Sphere Cave Folder POIs not being used

====================================================Version 8.3.2 Release=========================================================


v8.3.2.1

-Iced Tea Should Now Display Wellness
-Fixed Broken Issues with Wellness Option in Menu
-Fixed Missing Unlock Info On Farming Table
-Fixed Wrong Volume Numbering on Spears Books
-Fixed Spear Unlocks in Book Progression
-Fixed Irrigation Pump Not unlocking
-Added Apache Helicopter Recipe
-Complete Rework of the Wandering Horde System. Gamestaged to 1000, less Bears, more Vanilla Zombies and Tiers Added
-Rework of the Blood Moon System. Added Tiered Zombies at Late Gamestages
-Added Tiered Tom Clark Zombie
-Added Missing Ranger Book Craft to Research Desk

====================================================Version 8.3.3 Release=========================================================

v8.3.3.1

-Iced Tea Should Now Display Wellness
-Fixed Chemistry Station Recipe to Match Quest
-Fixed Blacksmith Forge Recipe and Quest Requirements to Match
-Fixed Radios can not be picked up
-Added Bricks to Brick Stack
-Increased Cobble and Cement Harvest on Pallets by 1-2
-Reduced Bird Nests Spawns
-Fixed Wrong Unlock Info for Farm Table
-Corrected Unlock Info for Scrap Knife
-Jumping Mutant Fix by Yakov
-Red Border Around Tools and Weapons Below 20 Percent Reinstated by Redbeard
-Stoves Are Now Repairable
-Chicken Coops Are Now Interactable
-Rabbit Cage Model Updated So It Is Interactable
-Cannabis Grow Time Now Matches Other Crops
-Cannabis Can Now Be Fertilized
-Added Station Tools to Air Drops
-Added Bicycle Parts to Air Drops
-Increased Count of Loot on Air Drops
-Added Ammo to Air Drops
-Removed Recipes for Wood and Steel Bars Trick
-Added Berry Bush Model That Spawns in the Forest and Gives Berries
-Added New Trash Bin Models to Replace the Ugly Green One
-Reduced Tree Stump Spawns Slightly
-Reduced Ore Node Spawns
-Added to the Wellness Tip
-Reduced timer on crafting Burning Barrel
-Reduced Cook Time on Murky Water in Can in Campfire
-Removed and Replaced Unused Snow Group with Lumberjack Group in 23 POIs
-Reduced Vanilla Crop Harvests from 2 to 1 to match Ravenhearst Crops
-Added a .5 Probability to get seed back from all Ravenhearst Crops to Match Vanilla
-Added 10x10 XL Armored Chest Storage Made With Tungsten
-Added Craftable Reinforced Chain Link Fence Set to Enable Actual Use for Bases with Higher HP
-Removed Individual Building Shape Recipes
-Added New Model for Writable XL Storage
-Added Missing Bowls to Recipes Needing Them
-Fixed Book Bundle Nulling Frome Removed Books
-Fixed Tier 5 POIs Treasure Chests Giving All Low Quality Items
-Fixed Improper Loot Quality That Did Not Honor Loot Gamestage
-Fixed Colliders on Mechanics Bench
-Fixed rh_industrial_Lumber_Mill(by IceJudge) Quest Issue

====================================================Version 8.3.4 Release=========================================================

v8.3.4.1

-Fixed Wrong Resource given back on Pallet of Feathers
-Added Multiple Taco Recipes
-Added Bowl to Shorty Stew
-Fixed Text on Backpacks
-Added Tactical Weapons to Progression and Action Skills to Fix Quality
-Drinking Infected Water Now Removes 2 Wellness
-Added Spawn Free Terrain. Terrain that when placed will not spawn zombies on it. Rewarded as schematic when you do the end game quest
-Reduced chances of Helicopter in loot at T5 Chests
-Crafting Leather from Animal Hide in the Tanning Rack now yields 2 Leather
-Added special descriptions to end tier ammo stating they can not be crafted
-Night Terrors Tyrannopode and Infernal Dog Along With Other Terrors Now Working Thanks to Yakov. (These may still have animation issues in MP)
-Clarified text on Light and Heavy Armor Durability Stat
-Re-Added Combine feature thanks to Redbeard
-Removed Knowledge Points Completely from Loot. Scrap Schematics For Them.
-Removed Ink from Lockers


====================================================Version 8.3.5 Release=========================================================

v8.3.5.1

-Fixed Null Spam on Terrors
-Fixed SlavCo Quest
-Removed Some Vehicles from SlavCo
-Fixed Vista Apartments Quest
-Resized Sleeper Triggers in Vista
-Deco and Terrain Updates to Vista
-Dropped Ministers Tree House to Tier 4
-Lowered Difficulty of Zombies in Tree House
-Fixed Quests in KFZ 02
-Fixed Buried Quest Marker in Tesco
-Fixed Sleeper Triggers in Haus 05
-Fixed Sleeper Triggers in KFZ 02
-Fixed Sleeper Triggers in Boxing Gym
-Added Missing Sleeper Triggers Preventing Zombies From Waking Up in Boxing Gym
-Fixed Water Values not Showing on Certain Foods
-Fixed Stick Pallets
-Fixed Some Localization Misspellings
-Flipped Unlock Perk Levels for Rain Catcher and Bee Hive
-Raised XP Needed to Level Slightly
-Increased Bread Yield When Crafting to 2
-Increased Counts of Milk and Mayo in Loot
-Upgrading to Final Shape of Stainless Steel Now Costs 20 Polish
-Added Dev Wellness Cake for Testing Purposes
-Added Dev Action Skill Books for Testing Purposes
-Classes Now Come With 2 Repair Items Included for the First Weapon


