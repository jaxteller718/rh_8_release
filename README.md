.. ***Jax Teller*** *and* ***The Ravenhearst Team present*** ...

# 🧟‍♀️ ***Ravenhearst 8.5*** 🧟
---

* Website: http://ravenhearst.enjin.com/
* GitLab: [@JaxTeller718](https://gitlab.com/JaxTeller718)

## Show your support

<a href="https://www.patreon.com/ravenhearstmod">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

### Features & Content :zap:

- Updated Sphercore, NPC Pack, RH Core, Speed Splash and Menu (*Sphere*, *Xyth*, *Yakov*, *Redbeard*)
- Added 5 percent chance of Dysentery to raw eggs (*Yakov*)
- Removed infection cures from bandages (*Yakov*)
- Bandit AI and Tasks Updated for Optimizations
- Added Recycler that takes metallic items. Drag and drop into them to break down into units. Can be found in Gas Stations, factories and workshops.
- Fixed Lighting on Tanning Rack and Rain Collectors (*Yakov*)
- Added Dented Cans of food that run the risk of dysentery
- Lessened Small Stone in the World
- Lessened Short Grass in the World
- Increased Tall Grass in the World
- Reduced Count on Air Drop Loot
- Reduced Count of Shapes Loot in Air Drops
- Increased Fog in World Slightly
- Darker Nights and Insides Added
- Added Drawbridges by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection  https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/)
- Added multitiered feral vanilla zombies
- Added hand picked Robleto Zombies to all spawns
- Added [OCB Mod Stop Fuel Waste](https://github.com/OCB7D2D/OcbStopFuelWaste)
- Added New Irrigation System
- Added Hydroponic Underground Farming by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection) and [here](https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/).
- Added Fish Farms and Oven by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection) and [here](https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/).
- Added Mega Storage that is writable by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection) and [here](https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/).
- Added new models for Chicken Coops by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection) and [here](https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/).

### Added the following POIS edited and resized by *romper* and *Constip*:
<sup>(NEEDS TESTING TO ENSURE QUESTS WORK AND SPAWNS CORRECTLY)</sup>
- RH_House_Burnt_Trap (by *Knightrath*)
- RH_House_Burrows (by *Nauti_Angel*)
- RH_House_Cabin (by *Sudo*)
- RH_House_Cabin_Hideout (by *TopMinder_Pille*)
- RH_Attr_Pistol_Gunshop (by *Magoli*)
- RH_Food_Taco_Bele (by *Swolk*)
- RH_Food_Truck_01 (by *LazMan*)
- RH_Food_Truck_Stop (by *Magoli*)
- RH_Govt_CDC_Mission (by *TheFootpad*)
- RH_Govt_Fire_Department (by *Guppycur*)
- RH_Govt_Fire_House (by *Warsaken*)
- RH_Hotel_Bar_DeadDog_Saloon (by *Libby*)
- RH_Hotel_Hostel (by *TopMinder*)
- RH_Hotel_Lakeside_Lodge (by *Kam*)
- RH_Hotel_Motel (by *Limodor*)
- RH_Hotel_National_Park_Lodge (by *LazMan*)
- RH_Hotel_Overlook (by *Quagmire1428*)
- RH_House (by *Yakov*)
- RH_House_A_Frame04p (by *Nodabba*)
- RH_House_A_Frame05p (by *Nodabba*)
- RH_House_Abandoned_11 (by *Deverezieaux*)
- RH_House_Amityville_Horror (by *Swolk*)
- RH_House_Apts_The_Sect_Of_M (by *magoli*)
- RH_House_At_The_Lake (by *Rukminesh*)
- RH_House_Blue (by *TopMinder_Pille*)
- RH_House_Brownstones (by *Laz_Man*)
- RH_House_Burnt_Shell (by *Evilracc0on*)
- RH_Church_01 (by *LazMan*)
- RH_Church_L4D_DT (by *CraterCreator*)
- RH_Farm_BerryPatch (by *Sinder*)
- RH_Farm_Green_House_01 (by *Fleshus*)
- RH_Farm_House_Small (by *Sinder*)
- RH_Farm_House_The_Mill (by *MoNKeYest1*)
- RH_Food_Cantine (by *Limodor*)
- RH_Food_Canucks_Coffee (by *NaGG*)
- RH_Food_Coffee_Shop (by *Evilracc0on*)
- RH_Food_DDnuts (by *RiahRex*)
- RH_Food_Hicks_Family_Diner (by *Hydro*)
- RH_Food_LMART_Road_House_Truckstop (by *Stallionsden*)
- RH_Food_McDowells (by *Laz_Man*)
- RH_Food_RedCat_Diner (by *Najax*)
- RH_Food_Restaurant_Italo (by *Axebeard*)
- RH_Food_Seafood_Shop (by *Guppycur*)
- RH_Food_Small_Cafe (by *Bostonlondon*)
- RH_Food_Snack_Shack (by *Bigstep70*)
- RH_Food_Steakhouse01 (by *Dragoness*)
- RH_Food_Subday (by *Swolk*)
- RH_House_Cabin_L4D_DT (by *CraterCreator*)
- RH_House_Settlement_WulftownCD (by *WereWulfen*)
- RH_House_Timberframe (by *Horst*)
- RH_House_WWHouse (by *IceJudge*)
- RH_Industrial_Oil_Well (by *Rukminesh*)
- RH_Industrial_Rick_Danger_Mine (by *Rick*)
- RH_Jail_Walking_Dead_Prison_TWD (by *LazMan*)
- RH_Medieval_Tower (by *VitaminE*)
- RH_Settlement_Art_Compound (by *Dmc*)
- RH_Settlement_House_Quiet_Pines (by *Hernan*)
- RH_Settlement_Shanty_Town (by *Evilracc0on*)
- RH_Settlement_Spanish_Mission_TheCure (redo by *Sinder*)
- RH_Settlement_TWD_Woodbury (by *SurvivorAndy*)
- RH_Store_Bookstore (by *Kayido*)
- RH_Store_Book_Stop (by *Hydro*)
- RH_Business_Corner_Diner (by *NLBEagle_RomperEdit*)
- RH_Business_Corner_Gunstore (by *NLBEagle_RomperEdit*)
- RH_Business_Corner_Pizza (by *NLBEagle_RomperEdit*)
- RH_House_Safehouse (by *Limodor*)
- RH_House_SindersHouse_V1 (by *Sinder*)
- RH_House_SindersHouse_V2 (by *Sinder*)
- RH_House_Small (by *TheTruJames*)
- RH_House_Split_Level (by *CraterCreator*)
- RH_House_Suburb_01 (by *LazMan*)
- RH_House_The_Home (by *Riahbug08*)
- RH_House_Wood_Small (by *TopMinder*)
- RH_House01 (by *Guppycur*)
- RH_House02 (by *Guppycur*)
- RH_House2 (by *Curbolt*)
- RH_House03 (by *Guppycur*)
- RH_House04 (by *Guppycur*)
- RH_House05 (by *Guppycur*)
- RH_Housewhiteblue (by *Owl79*)
- RH_HouseXIV_ByKam (redo by *Wiz_Sinder*)
- RH_House_City_House_3 (by *Kam*)
- RH_House_Cliff_House (by *War3zuk*)
- RH_House_Cottage (by *Fortbrick*)
- RH_House_CQC (by *Evilracc0on*)
- RH_House_Cubish (by *Evilracc0on*)
- RH_House_DaveHome2 (by *DMC*)
- RH_House_Duplex_Old_01_Blue (by *Deverezieaux*)
- RH_House_Duplex_Old_03_Brown (by *Deverezieaux*)
- RH_House_Duplex_Old_05_Burned (by *Deverezieaux*)
- RH_House_Ex_Farm_Stead (by *Owl79*)
- RH_House_Experimental (by *Tom*)
- RH_House_Hobbit (by *Limodor*)
- RH_House_Knolby_Cabin_1981 (by *Klaus0Engel*)
- RH_House_Kuldin_Brick (redo by *Sinder*)
- RH_House_LB_001 (by *Evilracc0on*)
- RH_House_LB_002 (by *Evilracc0on*)
- RH_House_LB_003 (by *EvilRacc0on*)
- RH_House_Lighthouse (by *NaGG*)
- RH_House_Modern_01 (by *Kam*)
- RH_House_Modern_04by_Kam (redo by *Robear*)
- RH_House_Modern_05by_Kam (redo by *Robear*)
- RH_House_Old_Curbolt (redo by *Sinder*)
- RH_House_Rubble_Cult (by *Evilracc0on*)

# Fixes & Changes 👾

- Fixed stats on soda can
- Fixed incorrect repair kit info on Steel Bat
- Added Fine Arrow Shafts crafted from plastic. They are used in steel and exploding and flaming arrows and bolts.
- Reduced weight on recipes and books for less KP per scrap
- Fixed incorrect unlock info on Javelin
- Health Drink now craftable in Stove
- Lowered chances of pots and grills in loot
- Fixed Junk Turret not giving XP to Electric Weapons
- Tools/Melee/Armor now loot to 299 Only
- Fixed Quality Issues on Hazmat Suits
- Added [Expanded Deco Modlet to Decor Bench](https://www.nexusmods.com/7daystodie/mods/607) by Vedui
- Added [Over 150 Storage Option to Decor Bench by Twunec](https://www.nexusmods.com/7daystodie/mods/2013)
- Increased Zombie Spawns and respawn timers worldwide
- Lowered yield on berry bushes
- Created New Zombie Bleed Buff. Vanilla Bleeds are 20 secs on first hit, 20 on additional. New Buff is 10 Seconds on 1st hit, 5 on additional.
- Added Lighted and Normal Drawbridges and Walk Ways in multiple sizes and lengths by *Arramus* and *Oakraven*. Find other modlets and the entire overhaul Oakraven [here](https://www.moddb.com/mods/oakraven-forest-collection) and [here](https://community.7daystodie.com/topic/27806-a20-oakraven-forest-collection/)
- Replaced all Burning Blue Flames in Tier 5 Radiated POIs with Yellow Fires
- Updated BepinEx
- Added [H7SB Seats](https://www.nexusmods.com/7daystodie/mods/2191)
- Added [H7SB Fishing](https://www.nexusmods.com/7daystodie/mods/2189)
- Added [H7SB Carts](https://www.nexusmods.com/7daystodie/mods/2185)
- Added [Show Reaming Clear Area](https://www.nexusmods.com/7daystodie/mods/2208)
- Added the ability to pick locked doors
- Pruned loot in Minister House
- Added Recycler and Ammo Bench to Minister House
- Changed Animal Breeding. Now you must acquire Snake Poison from snakes to make Tranquilizer Darts. Darts are used in Bows only (not crossbows). Hit the chicken or rabbit with a tranq, chase them down and punch them until they fall over. Use a Chicken Cage or Rabbit Cage on the body and you receive 1 chicken or rabbit. 2 of each are required for breeding.
- Changed Composting. Now the composter is a station. Food, organic materials and Crops can be put into the composter to make units of compost. Units of compost are used to craft Fertilizer and other items.
- Recycler now works like composter. Place proper items in the chute grid and it will recycle the materials to be made into brass, lead etc
- Re-Added Rebar Frame crafting to Blacksmith Forge
- Open grills can now be used to cook food in the world.
- Charcoal can now be crafted in Open Grills
- Water Filtration Unit recipes now use Charcoal.
- Added Composters to Farms
- Added Recyclers to Factories and Recyclers and Hauliers and Gas Stations
- Added Auto Bench Helper to Auto Stores and select Gas Stations
- Removed the need to craft chapters for the Journal. Replaced by a tape recorder with quests in it to purchase for 1 Duke. This will allow those who need to reacquire quests to do so instead of losing them forever.
- Added [Select Creatures from Oblivion Mods](https://www.nexusmods.com/7daystodie/mods/2228) (*pipermcleod*)
- Increased Durability on Primitive Stone Tools and Weapons
- Added Tarps to Harvest on Tarp Pallets and Fences
- Lessened Berry Bushes and Wells in the world
- Removed Treasure Artifacts
- Added *Redbeard's* fixes for Composter and Recycler Timers on Servers
