Key,Source,Context,Changes,English

vehicleBoxTruckbody,items,Part,EnChanged,Box Truck Body,,,,,
vehicleBoxTruckbodyDesc,items,Part,EnChanged,Parts needed to craft the Box Truck.,,,,,
vehicleBoxTruckchassis,items,Part,EnChanged,Box Truck Chassis,,,,,
vehicleBoxTruckchassisDesc,items,Part,EnChanged,Parts needed to craft the Box Truck.,,,,,
vehicleBoxTruckPlain,vehicles,vehicle,new,Box Truck,,,,,
vehicleBoxTruckPlainplaceable,vehicles,item,EnChanged,Box Truck,,,,,
vehicleBoxTruckPlainplaceableDesc,vehicles,item,EnChanged,An old Box Truck.,,,,,
vehicleBoxTruckHostess,vehicles,vehicle,new,Hostess Box Truck,,,,,
vehicleBoxTruckHostessplaceable,vehicles,item,EnChanged,Hostess Box Truck,,,,,
vehicleBoxTruckHostessplaceableDesc,vehicles,item,EnChanged,Tallahassee's favorite!,,,,,



vehicleGolfCart,vehicles,vehicle,new,Golf Cart,,,,,
vehicleGolfCartchassis,items,Part,EnChanged,Golf Cart Chassis,,,,,
vehicleGolfCartchassisDesc,items,Part,EnChanged,Parts needed to craft the Golf Cart.,,,,,
vehicleGolfCartaccessories,items,Part,EnChanged,Golf Cart Accessories,,,,,
vehicleGolfCartaccessoriesDesc,items,Part,EnChanged,Parts needed to craft the Golf Cart.,,,,,
vehicleGolfCartplaceable,vehicles,item,EnChanged,Golf Cart,,,,,
vehicleGolfCartplaceableDesc,vehicles,item,EnChanged,It might not be much but it's better than walking!,,,,,
meleeWpnClubT3GolfClub,items,Melee,KgNone,Golf Club,,,,,
meleeWpnClubT3GolfClubDesc,items,Melee,KgNone,No golf balls? There's always plenty of zombies to practice that swing on!,,,,,
meleeWpnClubT3GolfClubRHSchematic,items,Melee,KgNone,Golf Club Recipe,,,,,



vehicleWillyJeepRed,vehicles,vehicle,new,Willy Jeep,,,,,
vehicleWillyJeepRedplaceable,vehicles,item,EnChanged,Willy Jeep,,,,,
vehicleWillyJeepRedplaceableDesc,vehicles,item,EnChanged,If it survived WW2 surely it can handle this!,,,,,



vehicleWorkTruckchassis,items,Part,EnChanged,Work Truck Chassis,,,,,
vehicleWorkTruckchassisDesc,items,Part,EnChanged,Parts needed to craft the Work Truck.,,,,,
vehicleWorkTruckaccessories,items,Part,EnChanged,Work Truck Accessories,,,,,
vehicleWorkTruckaccessoriesDesc,items,Part,EnChanged,Parts needed to craft the Work Truck.,,,,,
vehicleWorkTruck,vehicles,vehicle,new,Work Truck,,,,,
vehicleWorkTruckplaceable,vehicles,item,EnChanged,Work Truck,,,,,
vehicleWorkTruckplaceableDesc,vehicles,item,EnChanged,Looks like it's been through more than just an apocalypse...,,,,,



vehicleDirtBike,vehicles,vehicle,new,Dirt Bike,,,,,
vehicleDirtBikeplaceable,vehicles,item,EnChanged,Dirt Bike,,,,,
vehicleDirtBikeplaceableDesc,vehicles,item,EnChanged,A dirt bike. And no neighbors to complain about it. Sweet!,,,,,
vehicleDirtBikeChassis,items,Part,EnChanged,Dirt Bike Chassis,,,,,
vehicleDirtBikeChassisDesc,items,Part,EnChanged,The beginning of something...awesome!,,,,,
vehicleDirtBikeParts,items,Part,EnChanged,Dirt Bike Parts,,,,,
vehicleDirtBikePartsDesc,items,Part,EnChanged,You might need these.,,,,,

vehicleChargerchassis,items,Part,EnChanged,Charger Chassis,,,,,
vehicleChargerchassisDesc,items,Part,EnChanged,Parts needed to craft the Charger.,,,,,
vehicleChargeraccessories,items,Part,EnChanged,Charger Accessories,,,,,
vehicleChargeraccessoriesDesc,items,Part,EnChanged,Parts needed to craft the Charger.,,,,,
vehicleChargerblack2,vehicles,vehicle,new,Charger,,,,,
vehicleChargerblack2placeable,vehicles,item,EnChanged,Charger,,,,,
vehicleChargerblack2placeableDesc,vehicles,item,EnChanged,Classic...,,,,,
vehicleChargerrust,vehicles,vehicle,new,Charger,,,,,
vehicleChargerrustplaceable,vehicles,item,EnChanged,Charger,,,,,
vehicleChargerrustplaceableDesc,vehicles,item,EnChanged,Classic...,,,,,
vehicleChargerrust2,vehicles,vehicle,new,Charger,,,,,
vehicleChargerrust2placeable,vehicles,item,EnChanged,Charger,,,,,
vehicleChargerrust2placeableDesc,vehicles,item,EnChanged,Classic...,,,,,

vehicleJunker,vehicles,vehicle,new,Junker,,,,,
vehicleJunkerplaceable,vehicles,item,EnChanged,Junker,,,,,
vehicleJunkerplaceableDesc,vehicles,item,EnChanged,An old custom bike.,,,,,
vehicleJunker2,vehicles,vehicle,new,Junker,,,,,
vehicleJunker2placeable,vehicles,item,EnChanged,Junker Purple,,,,,
vehicleJunker2placeableDesc,vehicles,item,EnChanged,An old custom bike.,,,,,
vehicleJunkerChassis,items,Part,EnChanged,Junker Chassis,,,,,
vehicleJunkerChassisDesc,items,Part,EnChanged,The beginning of something...awesome!,,,,,
vehicleJunkerbars,items,Part,EnChanged,Junker Handlebars,,,,,
vehicleJunkerbarsDesc,items,Part,EnChanged,You might need these.,,,,,

vehicleCruiser,vehicles,vehicle,new,Cruiser,,,,,
vehicleCruiserplaceable,vehicles,item,EnChanged,Cruiser,,,,,
vehicleCruiserplaceableDesc,vehicles,item,EnChanged,A custom Cruiser. Sweet!,,,,,
vehicleCruiser2,vehicles,vehicle,new,Cruiser,,,,,
vehicleCruiser2placeable,vehicles,item,EnChanged,Cruiser Faded,,,,,
vehicleCruiser2placeableDesc,vehicles,item,EnChanged,A custom Cruiser. Sweet!,,,,,
vehicleCruiserChassis,items,Part,EnChanged,Cruiser Chassis,,,,,
vehicleCruiserChassisDesc,items,Part,EnChanged,The beginning of something...awesome!,,,,,
vehicleCruiserbars,items,Part,EnChanged,Cruiser Handlebars,,,,,
vehicleCruiserbarsDesc,items,Part,EnChanged,You might need these.,,,,,

vehicleRat,vehicles,vehicle,new,Rat,,,,,
vehicleRatplaceable,vehicles,item,EnChanged,Rat,,,,,
vehicleRatplaceableDesc,vehicles,item,EnChanged,A custom Rat bike.,,,,,
vehicleRat2,vehicles,vehicle,new,Rat,,,,,
vehicleRat2placeable,vehicles,item,EnChanged,Rat Red,,,,,
vehicleRat2placeableDesc,vehicles,item,EnChanged,A custom Rat bike.,,,,,
vehicleRatChassis,items,Part,EnChanged,Rat Chassis,,,,,
vehicleRatChassisDesc,items,Part,EnChanged,The beginning of something...awesome!,,,,,
vehicleRatbars,items,Part,EnChanged,Rat Handlebars,,,,,
vehicleRatbarsDesc,items,Part,EnChanged,You might need these.,,,,,



vehicleOldSemichassis,items,Part,EnChanged,Old Semi Chassis,,,,,
vehicleOldSemichassisDesc,items,Part,EnChanged,Parts needed to craft the Old Semi.,,,,,
vehicleOldSemibody,items,Part,EnChanged,Old Semi Body,,,,,
vehicleOldSemibodyDesc,items,Part,EnChanged,Parts needed to craft the Old Semi.,,,,,
vehicleOldSemi,vehicles,vehicle,new,Old Semi,,,,,
vehicleOldSemiplaceable,vehicles,item,EnChanged,Old Semi,,,,,
vehicleOldSemiplaceableDesc,vehicles,item,EnChanged,"You better watch your step, Arlene!",,,,,


vehicleBoxTruckchassisSchematic,items,Part,EnChanged,Box Truck Chassis Schematic
vehicleBoxTruckbodySchematic,items,Part,EnChanged,Box Truck Body Schematic
vehicleChargerchassisSchematic,items,Part,EnChanged,Charger Chassis Schematic
vehicleChargeraccessoriesSchematic,items,Part,EnChanged,Charger Accessories Schematic
vehicleCruiserChassisSchematic,items,Part,EnChanged,Cruiser Chassis Schematic
vehicleCruiserbarsSchematic,items,Part,EnChanged,Cruiser Handlebars Schematic
vehicleDirtBikeChassisSchematic,items,Part,EnChanged,Dirt Bike Chassis Schematic
vehicleDirtBikePartsSchematic,items,Part,EnChanged,Dirt Bike Parts Schematic
vehicleGolfCartchassisSchematic,items,Part,EnChanged,Golf Cart Chassis Schematic
vehicleGolfCartaccessoriesSchematic,items,Part,EnChanged,Golf Cart Accessories Schematic
vehicleJunkerChassisSchematic,items,Part,EnChanged,Junker Chassis Schematic
vehicleJunkerbarsSchematic,items,Part,EnChanged,Junker Handlebars Schematic
vehicleOldSemichassisSchematic,items,Part,EnChanged,Old Semi Chassis Schematic
vehicleOldSemibodySchematic,items,Part,EnChanged,Old Semi Body Schematic
vehicleRatChassisSchematic,items,Part,EnChanged,Rat Chassis Schematic
vehicleRatbarsSchematic,items,Part,EnChanged,Rat Handlebars Schematic
vehicleWorkTruckchassisSchematic,items,Part,EnChanged,Work Truck Chassis Schematic
vehicleWorkTruckaccessoriesSchematic,items,Part,EnChanged,Work Truck Accessories Schematic
vehicleWorkTruckRHSchematic,items,Melee,EnChanged,Work Truck Schematic
vehicleWillyJeepchassis,items,Part,EnChanged,Willy Jeep Chassis
vehicleWillyJeepchassisDesc,items,Part,EnChanged,Parts needed to craft the Willy Jeep.
vehicleWillyJeepbody,items,Part,EnChanged,Willy Jeep Accessories
vehicleWillyJeepbodyDesc,items,Part,EnChanged,Parts needed to craft the Willy Jeep.
vehicleWillyJeepRHSchematic,items,Part,EnChanged,Willy Jeep Schematic