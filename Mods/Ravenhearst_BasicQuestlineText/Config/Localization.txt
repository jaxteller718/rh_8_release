﻿Key,English
quest_BasicSurvival1a,Basic Survival 5/11
quest_BasicSurvival1b,Basic Survival 3/11
quest_BasicSurvival1c,Basic Survival 2/11
quest_BasicSurvival1cc,Basic Survival 1/11
quest_BasicSurvival2a,Basic Survival 6/11
quest_BasicSurvival2b,Basic Survival 7/11
quest_BonusQuestTrash,Bonus Quest: Trash Bags
quest_BonusQuestBones,Bonus Quest: Bone Knife
quest_BonusQuestHoney,Bonus Quest: Honeycomb
quest_BonusQuestWater,Bonus Quest: Water Unit
