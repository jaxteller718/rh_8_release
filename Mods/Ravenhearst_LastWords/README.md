﻿# Ravenhearst_LastWords

[TOC]

This modlet does away with the generic "\<PlayerName\> died". Upon the death of a player, the modlet attempts to detect the way in which the player died and then sends a more appropriate global message with maybe a bit too much creativity sprinkled in.

## Testing Recommendations
- [ ] Verify that the following show relevant/appropriate death messages.
  - [ ] Player killed by NPC/zombie.
  - [ ] Player killed by a landmine.
  - [ ] Player killed by falling.
  - [ ] Player killed by radiation (biome OR cloud should work, but probably not both, see **Known Issues**).
  - [ ] Player killed by damage-over-time buff from an NPC attack (e.g. bleeding)
  - [ ] Player killed by an unmodded means (e.g. spike trap) gets the usual "Player died." message.

### Known Issues

It appears that the codebase only allows for one thing to produce 'radiation' damage, which translates to: Biome radiation vs Cloud radiation. Only one will produce the death messages, the other will produce "\<PlayerName\> died." Whether biome or cloud radiation gets the appropriate message depends on the XML element:

```xml
<damage_type value="radiation"/>
```

## Some Potential Extensions
- Add spike death messages.
- Add drowning death messages.
- Improve colorisation.

## Dependencies
- `7 Days To Die@^a20(b238)`