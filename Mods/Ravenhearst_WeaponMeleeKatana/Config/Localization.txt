﻿Key,Source,Context,English
meleeWpnBladeT3KatanaRH,items,Melee,Katana,,,,,
meleeWpnBladeT3KatanaRHDesc,items,Melee,"Great for slashing zombies and gutting animals for meat.\nRegular attacks cause 1 Bleeding Wound and power attacks at least 2.\nRepair with an Advanced Repair Kit.\nScrap to Iron.",,,,,
meleeWpnBladeT3KatanaRHSchematic,items,Melee,Katana Schematic,,,,,
